create table rango
(id_rango int primary key,
nivel varchar(20),
descripcion varchar(20)
);

create table bus
( id_bus int primary key,
numero_placa char(6),
capacidad int,
caracteristica varchar(20)
);

create table pasajero
(dni_pasajero char(8) primary key,
nombre varchar(20),
apellido_mat varchar(20),
apellido_pat varchar(20),
correo varchar(30),
direccion varchar(30),
telefono char(9)
);

create table empleado
(dni char(8) primary key,
nombre varchar(20),
apellido_mat varchar(20),
apellido_pat varchar(20),
contrasenha char(10),
id_rango int,
foreign key (id_rango) references rango (id_rango)
);

create table viaje
(id_viaje int primary key auto_increment,
id_bus int,
numero_asiento char(4),
lugar_origen varchar(20),
lugar_destino varchar(20),
fecha_viaje date,
hora_salida time,
tipo_asiento varchar(20),
foreign key (id_bus) references bus (id_bus)
);

create table venta
( id_venta int primary key auto_increment,
dni_pasajero char(8),
dni char(8),
fecha_venta date,
foreign key (dni_pasajero) references pasajero (dni_pasajero),
foreign key (dni) references empleado (dni)
);

create table boleto
(id_boleto int primary key auto_increment,
id_viaje int,
id_bus int,
id_venta int,
precio_boleto float,
foreign key (id_viaje) references viaje (id_viaje),
foreign key (id_bus) references bus (id_bus),
foreign key (id_venta) references venta (id_venta)
);