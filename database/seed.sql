INSERT INTO rango values (1, 'admnistrador', 'jefe');
INSERT INTO rango values (2, 'vendedor', 'encargado');

INSERT INTO bus values (1, 'A00001', 50, 'aire acondicionado');
INSERT INTO bus values (2, 'A00002', 50, 'cama 180 grados');
INSERT INTO bus values (3, 'A00003', 50, 'aire acondicionado');
INSERT INTO bus values (4, 'A00004', 50, 'wifi');
INSERT INTO bus values (5, 'A00005', 50, 'aire acondicionado');

INSERT INTO pasajero values ('75243578','milagros','padilla','sanchez', 'milagros.padilla@gmail.com','pasaje san miguel mz A','985632145');
INSERT INTO pasajero values ('75963259','carlos','rodriguez','francisco', 'carlosfr@gmail.com','los dominicos calle a ','964235879');
INSERT INTO pasajero values ('76985326','rodrigo','esquen','pariona', 'rodrigoes@gmail.com','las brisas mz h lt 12','963256987');
INSERT INTO pasajero values ('75369842','maricielo','parst','pariona', 'maricielopa@gmail.com','pasaje san miguel','953687452');
INSERT INTO pasajero values ('63598742','joaquin','peralta','saenz', 'joaquinper@gmail.com','pasaje las brisas mz q','963587423');

INSERT INTO empleado values ('78531559','jose', 'portilla', 'saenz', '123',1);
INSERT INTO empleado values ('68597432','marisol', 'lopez', 'braens', '235',2);
INSERT INTO empleado values ('53687426','camila', 'ñique', 'suel', '789',1);
INSERT INTO empleado values ('89617855','josefina', 'blas', 'jhons', '963',2);
INSERT INTO empleado values ('23684567','dolores', 'morantes', 'luso', '125',1);

INSERT INTO viaje values (1, 1, 'S001', 'Lima', 'Arequipa', '20-01-18', '2:00','normal');
INSERT INTO viaje values (2, 5, 'S002', 'Ica', 'lima', '11-11-18', '2:30','normal');
INSERT INTO viaje values (3, 2, 'S003', 'Lima', 'Iquitos', '18-04-18', '4:20','preferencial');
INSERT INTO viaje values (4, 2, 'S004', 'Junin', 'Arequipa', '23-05-17', '2:00','normal');
INSERT INTO viaje values (5, 4, 'S005', 'Loreto', 'Ica', '20-08-18', '7:00','preferencial');

INSERT INTO venta values (5, '63598742', '23684567', '18-08-02');
INSERT INTO venta values (4, '75369842', '89617855', '18-08-05');
INSERT INTO venta values (1, '75243578', '78531559', '18-08-12');
INSERT INTO venta values (2, '75963259', '68597432', '18-08-16');
INSERT INTO venta values (3, '76985326', '53687426', '18-08-23');

INSERT INTO boleto values (1, 1, 1, 1, 20.5);
INSERT INTO boleto values (2, 2, 4, 3, 12.35);
INSERT INTO boleto values (3, 1, 2, 5, 28.5);
INSERT INTO boleto values (4, 4, 5, 3, 23.5);
INSERT INTO boleto values (5, 1, 2, 5, 33.5);