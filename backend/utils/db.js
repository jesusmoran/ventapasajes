const mysql = require('promise-mysql');

let pool = mysql.createPool({
    host: process.env.DB_URL || 'localhost',
    port: process.env.DB_PORT || 3307,
    user: process.env.DB_USER || 'root',
    password: process.env.DB_PASS || 'root',
    database: process.env.DB_NAME || 'proyecto',
    connectionLimit: 10
});

pool.on('acquire', function (connection) {
    console.log('Connection %d acquired', connection.threadId);
});

const query = (sql) => {
    return pool.query(sql);
}

module.exports = query;