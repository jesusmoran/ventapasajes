const travelModel = require('../models/travel.model');

const getTravel = (req, res, next) => {

}

const getAllTravels = (req, res, next) => {
    travelModel.allTravels()
        .then(
            data => {
                res.send(data);
            },
            error => {
                console.error(error);
                res.status(500).send({
                    message: error
                });
            }
        )
}

const registerTravel = (req, res, next) => {

}

const updateTravel = (req, res, next) => {

}

const removeTravel = (req, res, next) => {

}


module.exports = {
    getTravel,
    getAllTravels,
    registerTravel,
    updateTravel,
    removeTravel,
}