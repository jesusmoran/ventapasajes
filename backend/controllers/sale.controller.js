const saleModel = require('../models/sale.model');

const insertSale = (req, res, next) => {
    const { user, employee, tickets } = req.body;
    console.log(user, employee, tickets);
    saleModel.registerSale(user, employee, tickets)
        .then(
            data => {
                res.send(true);
            },
            error => {
                console.error(error);
                res.status(500).send({
                    message: error
                })
            }
        ).catch(
            error => {
                console.error(error);
                res.status(500).send({
                    message: error
                })
            }
        );
}

const getAllSales = (req, res, next) => {
    saleModel.getSales()
        .then(
            data => {
                res.send(data)
            },
            error => {
                console.error(error);
                res.status(500).send({message: error});                
            }
        )
}

module.exports = {
    insertSale,
    getAllSales
}