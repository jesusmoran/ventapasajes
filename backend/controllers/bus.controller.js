const busModel = require('../models/bus.model');

const getAllBuses = (req, res, next) => {
    busModel.allBuses().then(
        data => {
            res.send(data);
        },
        error => {
            console.error(error);
            res.status(500).send({
                message: error
            });
        }
    )
}

const getBus = (req, res, next) => {
    const {id} = req.params;
    busModel.getBus(id).then(
        data => res.send(data),
        error => {
            console.error(error);
            res.status(500).send({
                message: error
            });
        }
    )
}

const insertBus = (req, res, next) => {
    const { bus } = req.body;

}

const updateBus = (req, res, next) => {

}

const removeBus = (req, res, next) => {

}

module.exports = {
    getAllBuses,
    getBus,
    insertBus,
    updateBus,
    removeBus
}