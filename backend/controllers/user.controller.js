const userModel = require('../models/user.model');

const login = (req, res, next) => {
    const {dni, password} = req.body;

    userModel.login(dni, password).then(
        data => {
            console.log(data);
            res.send(data);
        },
        error => console.error(error)
    );
    
}

const insertEmployee = (req, res, next) => {
    const { user } = req.body;

    userModel.insertUser(user).then(
        data => {
            console.log(data);
            res.send(data);
        },
        error => {
            console.error(error);
            res.status(500).send({
                message: error
            });
        }
    ).catch(
        error => res.status(500).send({message: error})
    );
    
}

const updateEmployee = (req, res, next) => {
    const {user} = req.body;
    console.log(user);
    userModel.updateUser(user).then(
        data => {
            console.log(data);
            res.send(data);
        },
        error => {
            console.error(error);
            res.status(500).send({
                message: error
            });
        }
    ).catch(
        error => res.status(500).send({message: error})
    );
}

const getEmployees = (req, res, next) => {
    userModel.allUsers().then(
        data => {
            res.send(data);
        },
        error => res.status(500).send({message: error})
    ).catch(
        error => res.status(500).send({message: error})
    );
}

const deleteEmployee = (req, res, next) => {
    const { id } = req.body;

    userModel.deleteUser(id).then(
        data => {
            res.send(data);
        },
        error => res.status(500).send({message: error})
    ).catch(
        error => res.status(500).send({message: error})
    );

}

module.exports = {
    login,
    getEmployees,
    insertEmployee,
    updateEmployee,
    deleteEmployee
}