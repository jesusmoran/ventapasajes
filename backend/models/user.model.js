const db = require('../utils/db');

const insertUser = function insertUser(user){
    const sql = `INSERT INTO empleado VALUES ('${user.dni}', '${user.nombre}', '${user.apellido_pat}', '${user.apellido_mat}', '${user.contrasenha}', ${user.id_rango})`;
    return db(sql);
}

const updateUser = function(user){
    const sql = `UPDATE empleado SET dni='${user.dni}', nombre='${user.nombre}', apellido_pat='${user.apellido_pat}', apellido_mat='${user.apellido_mat}', contrasenha='${user.contrasenha}', id_rango=${user.id_rango}
                    WHERE dni='${user.dni}'`;
    return db(sql);
}

const login = function verifyUser(dni, password){
    const sql = `SELECT * FROM empleado WHERE dni='${dni}' AND contrasenha='${password}'`;
    return db(sql);
}

const allUsers = function getAllUsers(){
    const sql = 'SELECT * FROM empleado';
    return db(sql);
}

const deleteUser = function deleteUser(dni){
    const sql = `DELETE FROM empleado WHERE dni='${dni}'`;
    return db(sql);
}

module.exports = {
    login,
    allUsers,
    insertUser,
    updateUser,
    deleteUser
}