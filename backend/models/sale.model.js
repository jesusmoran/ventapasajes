const db = require('../utils/db');

const passengerModel = require('./passenger.model');
const ticketModel = require('./ticket.model');

const registerSale = (client, employee, tickets) => {        

    let contTickets = 0;
    
    return new Promise((resolve, reject) => {
        passengerModel.savePassenger(client)
        .then(
            cl => {
                console.log(client);
                const sqlSale = `INSERT INTO venta(dni_pasajero, dni, fecha_venta) VALUES ('${client.dni}', '${employee.id}', CURRENT_TIME());`;
                console.log(sqlSale);
                db(sqlSale).then(
                    sale => {
                        console.log(sale);

                        for(let i=0; i<tickets.qt; i++){
                            ticketModel.saveTicket(sale.insertId, tickets.info).then(
                                ticket => {
                                    contTickets++;
                                    if(contTickets == tickets.qt){
                                        resolve(true);
                                    }
                                },
                                err => reject(err)
                            );
                        }
                        
                    },
                    err => {
                        reject(err);
                    }
                );
            },
            err => {
                reject(err);
            }
        );        
    });
    
}

const getSales = () => {
    const sql = `SELECT V.id_venta, P.nombre as pasajero_nombre, E.nombre emp_nombre, V.fecha_venta
                FROM venta V inner join empleado E on V.dni=E.dni
                inner join pasajero P on V.dni_pasajero= P.dni_pasajero;
    `;
    return db(sql);
}

module.exports = {
    registerSale,
    getSales
}