const db = require('../utils/db');

const allBuses = function getAllBuses(){
    const sql = 'SELECT * FROM bus';
    return db(sql);
}

const getBus = function getOneBus(id){
    const sql = 'SELECT * FROM bus WHERE id_bus='+id;
    return db(sql);
}

module.exports = {
    allBuses,
    getBus
}