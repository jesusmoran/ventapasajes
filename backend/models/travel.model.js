const db = require('../utils/db');

const allTravels = function getAllTravels(){
    const sql =  'SELECT * FROM viaje';
    return db(sql);
}

module.exports = {
    allTravels
}