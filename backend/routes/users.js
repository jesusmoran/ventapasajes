var express = require('express');
var router = express.Router();

const userCtrl = require('../controllers/user.controller');

router.post('/', userCtrl.login);

module.exports = router;
