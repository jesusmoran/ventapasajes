var express = require('express');
var router = express.Router();

const travelCtrl = require('../controllers/travel.controller');
const busCtrl = require('../controllers/bus.controller');
const employeeCtrl = require('../controllers/user.controller');
const salesCtrl = require('../controllers/sale.controller');

router.get('/', function(req, res, next) {
  res.send({ status: 'API is up!' });
});

//Travels CRUD
router.get('/travel/:id', travelCtrl.getTravel);
router.get('/travel', travelCtrl.getAllTravels);
router.post('/travel', travelCtrl.registerTravel);
router.put('/travel', travelCtrl.updateTravel);
router.delete('/travel', travelCtrl.registerTravel);

//Buses CRUD
router.get('/bus/:id', busCtrl.getBus);
router.get('/bus', busCtrl.getAllBuses);
router.post('/bus', busCtrl.insertBus);
router.put('/bus', busCtrl.updateBus);
router.delete('/bus', busCtrl.removeBus);

//Employees CRUD
router.get('/employees', employeeCtrl.getEmployees);
router.post('/employees', employeeCtrl.insertEmployee);
router.put('/employees', employeeCtrl.updateEmployee);
router.delete('/employees', employeeCtrl.deleteEmployee);

//Sales CRUD
router.get('/sales', salesCtrl.getAllSales);
router.post('/sales', salesCtrl.insertSale);

module.exports = router;