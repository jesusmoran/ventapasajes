import Vue from 'vue'
import Router from 'vue-router'

import Login from './components/Login';
import Sale from './components/Sale';
import SalesList from './components/SalesList';
import TravelRegister from './components/TravelRegister';
import EmployeeRegister from './components/EmployeeRegister';

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import(/* webpackChunkName: "about" */ './components/Dashboard.vue'),
      children: [
        {
          path: '',
          component: Sale
        },
        {
          path: 'sales',
          component: SalesList
        },
        {
          path: 'travel',
          component: TravelRegister
        },
        {
          path: 'employees',
          component: EmployeeRegister
        }
      ]
    }
  ]
})
